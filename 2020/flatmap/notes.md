# Technical requirements
- microphone
- projector that I can hook up to

# Why I'm the best person to speak on this subject
First and foremost, this is _my_ story (or to be fair, _my team's_ story). It is something I have lived and something I'd like to share.

Setting that aside, what makes me the right person to talk about this? I graduated university last spring. That was my introduction to programming. Throughout university, all we were introduced to was object oriented and imperative languages. About two years ago, however, my friend introduced me to functional programming. I fell in love.

After joining my current team, it seems that this enthusiasm has started to spread to my teammates. As our workplace restricts us to a subset of languages (of which none are really functional), the influence has started to seep in through other avenues.

In all, I'm still fairly new to FP and lack experience, but I can see the power that it holds (and so can my teammates). But with great power comes great responsibility, and maybe we haven't earned that just yet. However, we're always learning, and always building towards a better tomorrow, so this is a great chance to see just how people with an object oriented background think about functional programming in an object oriented language.

# Previous relevant experience

While I might not have much experience from the tech conference circle specifically, I'm far from being new to the stage. Previous relevant experiences include:
  - Over 10 years as a performing musician, everything from musicals to dance shows, to rock and pop concerts.
  - Two years of experience as a group fitness instructor. Presenting stuff to people while doing burpees is a special kind of skill.
  - Community builder and leader of the developer 'show and tell' initiative at my current workplace, a weekly session where we present technical topics to other developers internally.
  - Co-organizer of the Oslo Rust Meetup
  - Done talks and workshops at several meetups in the Oslo area

In other words, I'm very comfortable being on stage, talking to people, and seeing people. Also, to assuage any potential fears about my English abilities, I'd like to point out that I did live in England for a number of years, and that I am very comfortable with the language.

Lastly, as I'm based in Oslo: In the event that you have any questions or things you'd like to see me about, you can always reach out and we could arrange a meeting.


# Previous publications
I blog regularly over at https://thomashartmann.dev. A lot of the content is quite functional-oriented, but to pick out a couple things, there's the [Rust posts](https://thomashartmann.dev/tags/rust/), the [Haskell posts](https://thomashartmann.dev/tags/haskell/) (mainly about learning Haskell), and, at least tangentially related, the [NixOS posts](https://thomashartmann.dev/tags/nixos/).

# Profile photo
I've sent an email to the provided address with a photo.

# Speaker's dinner: dietary needs
I'm allergic to shellfish, but everything else goes.
