This is a document containing ideas for talks for 2020.
* Elm as a gateway (drug?)                     :fp:haskell:elm:rust:learning:
  It's now been a year and a half since I fell into functional programming, and boy, what a journey it has been.
** The Haunted Hauskell
   Not as scary as it looks. Actually quite pleasant. Like that ghost you know that's actually super kind.
** Rusty Revelations
   Ah, so this is the power of Rust? A lot of the power of the type system had completely slipped past me, but Elm showed me what it's all about.
* Testing C# with F# (or: how to smuggle functional programming into your business) / Functional Testing of Object Oriented code :fp:oop:testing:fuzzing:
  - how is functional programming useful for testing?
  - fscheck
  - helps you write more isolated, more testable code (everyone says this)
  - how to get your fellow developers on board
* OpenShift for power developers
  Sure, OpenShift contains a +nice+ functional UI for 'developers', but some of use just want more control and want to do it from the command line
* Solving problems by contributing to open source :rust:foss:kubernetes:nushell:
  All the buzzwords: rust, open source, k8s.

  Actually just wanted to solve my problem. That is the *core* of the issue.

* Functional patterns for the object oriented programmer: a report from the trenches / stop `try`ing, start `do`ing / `do` or `do` not, there is no `try` :fp:c#:
  Stop try-catching, use the monadic `do` notation to do railway-oriented programming
  Using a homegrown Result type, my team managed to completely abstract away all try-catching. It's pretty good, and it allows us to write code in a really nice, concise readable manner. Most of the time. There's also dark sides.
* Emacs as a C#/TypeScript editor                               :emacs:c#:ts:
  Don't have much to say. Just use Spacemacs?

