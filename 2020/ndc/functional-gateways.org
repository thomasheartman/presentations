- Session title* :: What if I told you ... about Functional Programming
- Description* ::
                  Functional programming. Stop for a moment and taste those words. Savor them. How do they feel? Are you excited? Scared? Weary? No matter your stance, functional programming concepts are all the rage these days, all the way from the trenches of the JavaScript framework wars and through to the new, hot programming languages.

                  If you're feeling FP-curious, why not come along for a little tale of how I latched on to the white rabbit and found my way into a strange, new wonderland, full of new concepts to take with me back home---that is, if I ever wanted to leave. So come on in, join me, and together we'll find out just how deep the rabbit hole goes.

- Session format* :: +Lightning Talk+ | Session | +60 min workshop+ | +120 min workshop+
- Level* :: Beginner | Intermediate | Advanced | All levels
- Tags :: functional programming, haskell, elm, introduction
- Target audience* :: People who are curious about functional programming, who want to know what all the hype is about, and people who want an introduction to some of the basic principles of FP.
