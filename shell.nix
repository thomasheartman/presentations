{ pkgs ? import <nixpkgs> { } }:
with pkgs;
stdenv.mkDerivation {
  name = "presentations";
  buildInputs = [ nodejs ];
}
