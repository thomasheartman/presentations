#+Title: The Hitchhiker's Guide to Response Modeling
#+Author: Thomas Hartmann
#+Email: thomas.hartmann@intility.no
#+Mastodon: @hartmann@mastodon.technology

#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil reveal_loop:t
#+OPTIONS: reveal_width:1200 reveal_height:800
#+OPTIONS: toc:0
#+REVEAL_MARGIN: 0.1
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TITLE_SLIDE:../title.html
#+REVEAL_TRANS: fade
#+REVEAL_THEME: night
#+REVEAL_HLEVEL: 1
#+REVEAL_POSTAMBLE: <p> Created by Thomas Hartmann. </p>
#+REVEAL_ROOT: ../node_modules/reveal.js

* About yours truly
  - OO background (predominantly C#)
  - Worked a lot in JS, Python
  - Certified /Legacy Code Base Explorer/ ™
  - About a year ago, a friend introduced me to /Elm/ ...
#+begin_notes
I'd like to present my experience and how I ended up stumbling into this little rabbit hole.

Not trying to force this on anyone
Want to give this talk because I see it everywhere and because people don't tend to know what they are.
#+end_notes

* The problem
You work for a games company that runs a popular online multiplayer game. As usual, there are some bad actors, and you've decided to create a ban list, but it most follow a few rules:
- A ban is only valid for a set amount of time and can not be extended.
- For GDPR compliance, you cannot store the direct ID of a player, but a hash is okay.


** In summary:
   - Table columns:
     - ~id~
     - ~insertion_time~
   - In case of conflict: Do /not/ overwrite.
   - Only store hashes, no IDs.

* Solving it in C#
** The insertion service
    #+begin_src csharp
      public class InsertionService : IInsertionService
      {
          private readonly IRepository _repo;
          private readonly IHashingService _hasher;

          public InsertionService(IRepository repo, IHashingService hasher)
          {
              _repo = repo;
              _hasher = hasher;
          }

          public async Task<string> AddToBanList(Player player)
          {
              /* implement me 🤐 */
          }
      }
  #+end_src
  #+begin_notes
  Explain that the interfaces are what they think they are.
  #+end_notes
**  Possible solution

   #+begin_src csharp
        public async Task<string> AddToBanList(Player player)
        {
            var hashedId = _hasher.Hash(player.Id);

            if (await _repo.FindById(hashedId) != null)
            {
                throw new AlreadyExistsException();
            }

            await _repo.InsertRow(new TableRow
            {
                Id = hashedId,
                InsertionTime = DateTime.UtcNow
            });

            return hashedId;
        }
   #+end_src
*** This works
*** ... but is it /really/ an exception that an entry with this id already exists?

* Let's talk about exceptions 🦹
#+ATTR_REVEAL: :frag (appear)
  - They can hide everywhere and there's no way to know
    #+ATTR_REVEAL: :frag t
     /lol, catch this, motherfu*ker/ 😈 --- ~IDontThrow()~
  - The caller can choose to ignore exceptions completely
    #+ATTR_REVEAL: :frag t
     /I won't tell if you don't/ 😉 --- The compiler
  - They shouldn't be used for control flow ...
    #+ATTR_REVEAL: :frag t
     ...right? 🤫
** 😑

   #+begin_notes
   What if I told you there's a better way?
   #+end_notes

* What if you could return more than one type from a function?

** Enter: Algebraic Data Types

 #+begin_quote
 In computer programming [...] an algebraic data type is a kind of composite type, i.e., **a type formed by combining other types**. ---
 [[https://en.wikipedia.org/wiki/Algebraic_data_type][Wikipedia]]
 #+end_quote

 #+ATTR_REVEAL: :frag fade-in
    Also known as
 #+ATTR_REVEAL: :frag (fade-in-then-semi-out)
  - Custom types
  - Variant types
  - Sum types
  - Disjoint unions
  - Tagged unions
  - ++
#+begin_notes
  - Custom types
  - Variant types
  - Sum types
  - Disjoint unions
  - Tagged unions
#+end_notes
  #+reveal: split
  #+begin_src haskell
  data Maybe a
    = Nothing
    | Just a
  #+end_src

 #+begin_src haskell
  data Either a b
    = Left a
    | Right b
  #+end_src


  #+begin_src haskell
   data List a
     = Nil
     | Cons a
            (List a)
  #+end_src

   #+begin_src haskell
   data Tree a
     = Empty
     | Leaf a
     | Node (Tree a)
            (Tree a)
  #+end_src



* DON'T PANIC

**  In practice they're a lot like enums ...
 #+begin_src rust
   enum Result {
       Ok,
       Err,
   }
 #+end_src

** ... with superpowers!🦸

*** Each variant can have associated data:
  #+begin_src rust
  enum Result<T, E> {
     Ok(T),
     Err(E),
  }
  #+end_src

  #+begin_src rust
    enum Option<T> {
        Some(T),
        None,
    }
  #+end_src

#+begin_notes
These two are perhaps the most common ADTs around.

Result models things that can fail (sound familiar)?
Indeed! This is great for taking care of operations that could fail and replaces exceptions.

Option models the potential absence of a value.
Similar to ~null~ in C#, but explicit. Trust me, this stops a lot of bugs.

Using these two types, we can get rid of nulls  and exceptions completely
#+end_notes
*** And the compiler will help you:
    #+begin_src rust
      fn help_me_compiler<T>(optional: Option<T>) -> T {
          match optional {
              Some(value) => value,
              // we're not handling the None case here
          }
      }
    #+end_src
    #+begin_src
error[E0004]: non-exhaustive patterns: `None` not covered
  --> src/main.rs:10:11
   |
10 |     match optional {
   |           ^^^^^^^^ pattern `None` not covered
   |
   = help: ensure that all possible cases are being handled,
   possibly by adding wildcards or more match arms
    #+end_src
#+begin_notes
The keen eye will notice that even if we were to try and handle the ~None~ case here, there is no way we could return anything sensible. In other words, this code /can never compile/. (without leading to a crash)
#+end_notes
* Now, /this/ we can use!
#+begin_notes
Now that we can represent different states with different properties, let's revisit the problem.
#+end_notes

** Using ADTs to model the problem
   #+begin_src rust
      // ADT
      enum InsertionOutcome {
          Success(String),
          AlreadyExists,
      }

      // type alias
      type DbResult<T> = Result<T, ConnectionError>;
   #+end_src

#+ATTR_REVEAL: :frag appear
   #+begin_src rust
// Insertion
fn add_to_ban_list(
    repo: impl Repository,
    hash: &Fn(&str) -> &str,
    player: Player,
) -> DbResult<InsertionOutcome> {
    let hashed_id = hash(&player.id);
    match repo.find_by_id(&hashed_id)? {
        Some(_) => Ok(InsertionOutcome::AlreadyExists),
        None => repo
            .insert(hashed_id)
            .map(|_| InsertionOutcome::Success(hashed_id.to_string())),
    }
}

   #+end_src
#+begin_notes
That C# block from earlier can be converted to this little function. Take note of the question marks. Much like the null conditional operator from in C#, it will terminate early in case of an error. Note also that at the end, after ~repo.insert~, there is a ~map~ function. All Repository function return ~DbResult~, so you cannot just assume that you have a value. This leads us into the next big topic ...
#+end_notes

 #+reveal: split
   #+begin_src rust
    let hashed_id = hash(&player.id);
    match repo.find_by_id(&hashed_id)? {
        Some(_) => Ok(InsertionOutcome::AlreadyExists),
        None => repo
            .insert(hashed_id)
            .map(|_| InsertionOutcome::Success(hashed_id.to_string())),
    }
   #+end_src

* Working with the context
  #+begin_src rust
    fn greet(attendee: Attendee) -> String {
        match attendee {
            Attendee::Intility => "Hey, crew. How we doing?".to_owned(),
            Attendee::Person("Clement") => "Nice to see ya, buddy!".to_owned(),
            Attendee::Person(any_other_name) => format!("Hey, {}.", any_other_name),
        }
    }
  #+end_src


** Schrödingers types
 The ~Result~ and ~Option~ types put you in a context, a box of sorts.

Much like Schrödinger's Cat, you don't know which variant the value is until you look, and until then, you have to treat it as if it's both at the same time.

#+begin_src rust
  enum SchrodingerCat {
      Alive(u8),
      Dead,
  }

  fn check_cat(cat: SchrodingerCat) -> String {
      match cat {
          Alive(lives) => format!("The cat's got {} extra lives left.", lives),
          Dead => "The cat is dead.".to_owned()
      }
  }
#+end_src

 #+begin_notes
 And when we do check, we only have access to the value within that scope.

 This also means that any function where you get a value of either of the above types either need to return the same type or do a "lossy conversion"
 #+end_notes

** Lossy conversions
   Some conversions are lossless:
#+begin_src rust
  fn lives_left(cat: SchrodingerCat) -> u8 {
      match cat {
          Alive(lives_left) => lives_left + 1,
          Dead => 0
      }
  }

  fn from_lives(lives: u8) -> SchrodingerCat {
      match lives {
          0 => Dead,
          n => Alive(n-1)
      }
  }
#+end_src

 #+reveal: split
 Others are not.
#+ATTR_REVEAL: :frag fade-in

#+begin_src rust
fn from_division<E>(division_result: Result<f64, E>) -> f64 {
    match division_result {
        Ok(value) => value,
        Err(_) => panic!() // ... what do we do here? 0?
    }
}
#+end_src

#+begin_notes
For some values it makes sense to have a default (identity): lists, strings, numbers (addition/multiplication),

For others, not so much. This is why the context is important.
#+end_notes


* Using ADTs in C#
#+begin_notes
But enough about my rambling on about how they work. Let's see if we can somehow use it in C#!

Now I'll be the first to admit: it's not quite there yet. The ergonomics are not in place, and it's not idiomatic. /However/, some avenues are worth exploring.
#+end_notes

** The simple option
   Enums
   #+begin_src csharp
    public enum DatabaseInsertionResult
    {
        Updated,
        Ok,
        Error
    }

    public async Task<DatabaseInsertionResult> Add(Entry e)
    {
        try
        {
            if (exists(e))
            {
                // update record
                return DatabaseInsertionResult.Updated;
            }

            return DatabaseInsertionResult.Ok;
        }
        catch (Exception)
        {
            return DatabaseInsertionResult.Error;
        }
    }
   #+end_src

 #+begin_notes
 The easy option, especially if you don't care about a return value other than what type of response it was, is to just use standard enums.
 #+end_notes

** The more involved way
 #+begin_src rust
   enum Result<T, E> {
       Ok(T),
       Err(E),
   }
 #+end_src

 #+ATTR_REVEAL: :frag fade-in
 #+begin_src csharp
    public abstract class Result<T, E>
    {
        private Result() { } // so that no-one else can inherit this

        public sealed class Ok : Result<T, E>
        {
            private readonly T _value;
            public T Value => _value;
            public Ok(T ok) { _value = ok; }
        }

        public sealed class Err : Result<T, E>
        {
            private readonly E _value;
            public E Value => _value;
            public Err(E err) { _value = err; }
        }
    }
 #+end_src

 #+begin_notes
 We all know that developer ergonomics play a key part in whether something gets used or not, so let's have a little comparison
 #+end_notes

 #+reveal: split

 #+begin_src rust
 fn function<T, E>(result: Result<T, E>) -> &'static str {
     match result {
         Ok(_) => "Ok",
         Err(_) => "Err",
     }
 }
 #+end_src

 #+ATTR_REVEAL: :frag fade-in
 #+begin_src csharp
public static string Function<T, E>(Result<T, E> result)
{
    switch (result)
    {
        case Result<T, E>.Ok _ok:
            return "Ok";
        case Result<T,E>.Err _err:
            return "Err";
        default:
            return "Why do I need this again?";
    }
}
 #+end_src

 #+begin_notes
 Overall, in my experience, the developer ergonomics aren't great for this style of programming just yet, and that is a shame. However, with C# 8.0, it looks like it's getting quite a bit better, and it's clearly taking after the functional style where it's applicable.
 #+end_notes

** C# 8.0
   C#
#+begin_src csharp
static string Display(object o) => o switch
{
    Point(0, 0)         => "origin",
    Point(var x, var y) => $"({x}, {y})",
    _                   => "unknown"
};
#+end_src
Rust
#+begin_src rust
fn display(p: Point) -> String {
    match p {
        Point(0, 0) => "origin".to_owned(),
        Point(x, y) => format!("({}, {})", x, y)
    }
}
#+end_src

  #+reveal: split
C#
#+begin_src csharp
static State ChangeState(State current, Transition transition, bool hasKey) =>
    (current, transition) switch
    {
        (Opened, Close)              => Closed,
        (Closed, Open)               => Opened,
        (Closed, Lock)   when hasKey => Locked,
        (Locked, Unlock) when hasKey => Closed,
        _ => throw new InvalidOperationException($"Invalid transition")
    };
#+end_src
Rust
#+begin_src rust
fn change_state(current: State, transition: Transition, has_key: bool) -> Result<State, &'static str> {
    match (current, transition) {
        (Opened, Close)             => Ok(Closed),
        (Closed, Open)              => Ok(Opened),
        (Closed, Lock)   if has_key => Ok(Locked),
        (Locked, Unlock) if has_key => Ok(Closed),
        _ => Err("Invalid transition")
    }
}
#+end_src
 #+begin_notes
 C# 8.0 introduces a lot of what you need to effectively use what I have been outlining in this talk:
 - Nullable reference types :: You can now be (almost) certain that when you have an object reference, it's actually there and not ~null~. While it won't be enforced out of the gate (I think), this essentially gives you the ~Option<T>~ type, just on a more underlying language level. This has certain advantages and certain disadvantages. Advantages: it's less typing to say that you expect a ~T?~ than an ~Option<T>~, and ... that's it? Disadvantages, seeing as nullable isn't a type implementation in and of itself, you can't map over it in the same way, though it's likely that you can create extension methods for ~Nullable<T>~ which would let you have the same (monadic) powers.
 - Pattern matching :: C# has long had a ~switch~ construct but it is now getting a ~switch expression~ which both lets you skip the ~return~ keyword and also lets you write it in a less verbose manner. Furthermore, using ~deconstructors~, you can now create custom deconstructors for your types, letting you access the inner value in much the same way as the various constructors in Rust, Haskell, etc.
 #+end_notes


* Resources
  + [[https://www.youtube.com/watch?v=IcgmSRJHu_8][Richard Feldman: /Making Impossible States Impossible/ ]]
  + [[https://www.youtube.com/watch?v=E8I19uA-wGY][Scott Wlaschin: /Functional Programming Design Patterns/ ]] (see: Railway oriented programming)
