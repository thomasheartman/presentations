namespace response_modeling_csharp
{
    public static class ResultExample
    {
        public static string Function<T, E>(Result<T, E> result)
        {
            switch (result)
            {
                case Result<T, E>.Ok _ok:
                    return "Ok";
                case Result<T, E>.Err _err:
                    return "Err";
                default:
                    return "Why do I need this again?";
            }
        }
    }
}