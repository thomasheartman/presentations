using System.Threading.Tasks;

namespace response_modeling_csharp
{
    public interface IInsertionService
    {
        Task<string> AddToBanList(Player player);
    }
}
