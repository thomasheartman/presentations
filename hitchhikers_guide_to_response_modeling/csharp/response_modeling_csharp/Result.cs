namespace response_modeling_csharp
{
    public abstract class Result<T, E>
    {
        private Result() { } // so that no-one else can inherit this

        public sealed class Ok : Result<T, E>
        {
            private readonly T _value;
            public T Value => _value;
            public Ok(T ok) { _value = ok; }
        }

        public sealed class Err : Result<T, E>
        {
            private readonly E _value;
            public E Value => _value;
            public Err(E err) { _value = err; }
        }
    }
}