using System;

namespace response_modeling_csharp
{
    public interface IHashingService
    {
        string Hash(Guid guid);
    }
}
