using System;
using System.Threading.Tasks;

namespace response_modeling_csharp
{
    public interface IRepository
    {
        Task<TableRow> FindById(string id);
        Task InsertRow(TableRow row);
    }
}
