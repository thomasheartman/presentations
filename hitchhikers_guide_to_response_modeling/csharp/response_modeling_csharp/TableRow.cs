using System;

namespace response_modeling_csharp
{
    public class TableRow
    {
        public string Id { get; set; }
        public DateTime InsertionTime { get; set; }
    }
}
