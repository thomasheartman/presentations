using System;
using System.Threading.Tasks;

namespace response_modeling_csharp
{
    public class InsertionService : IInsertionService
    {
        private readonly IRepository _repo;
        private readonly IHashingService _hasher;

        public InsertionService(IRepository repo, IHashingService hasher)
        {
            _repo = repo;
            _hasher = hasher;
        }

        public async Task<string> AddToBanList(Player player)
        {
            var hashedId = _hasher.Hash(player.Id);

            if (await _repo.FindById(hashedId) != null)
            {
                throw new AlreadyExistsException();
            }

            await _repo.InsertRow(new TableRow
            {
                Id = hashedId,
                InsertionTime = DateTime.UtcNow
            });

            return hashedId;
        }
    }
}
