// note: this is only used to show what rust code looks like. It's useful to make sure it compile, but the program doesn't do anything useful.
#![allow(dead_code)]

extern crate chrono;

use chrono::prelude::*;
use std::error;
use std::fmt;

fn main() {
    println!("Hello, response modeling!");
    help_me_compiler(Some(42));
}

fn help_me_compiler<T>(optional: Option<T>) -> T {
    match optional {
        Some(value) => value,
        None => panic!(),
        // with the information the function has, there is no way we can resolve this.
    }
}

// Outcomes
enum InsertionOutcome {
    Success(String),
    AlreadyExists,
}

// Custom error
#[derive(Debug)]
struct ConnectionError {
    description: String,
    // imagine more fields on here if you want to.
}

impl fmt::Display for ConnectionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description)
    }
}

impl error::Error for ConnectionError {
    fn description(&self) -> &str {
        &self.description
    }

    fn cause(&self) -> Option<&error::Error> {
        None
    }
}

// Repository
type DbResult<T> = Result<T, ConnectionError>;

struct TableRow<'a> {
    id: &'a str,
    insert_time: DateTime<Utc>,
}

trait Repository {
    fn find_by_id(&self, id: &str) -> DbResult<Option<TableRow>>;
    fn insert(&self, id: &str) -> DbResult<()>;
}

// player
struct Player {
    id: String,
}

// Insertion
fn add_to_ban_list(
    repo: impl Repository,
    hash: &Fn(&str) -> &str,
    player: Player,
) -> DbResult<InsertionOutcome> {
    let hashed_id = hash(&player.id);
    match repo.find_by_id(&hashed_id)? {
        Some(_) => Ok(InsertionOutcome::AlreadyExists),
        None => repo
            .insert(hashed_id)
            .map(|_| InsertionOutcome::Success(hashed_id.to_string())),
    }
}

enum Attendee<'a> {
    Intility,
    Person(&'a str),
}

fn greet(attendee: Attendee) -> String {
    match attendee {
        Attendee::Intility => "Hey, crew. How we doing?".to_owned(),
        Attendee::Person("Clement") => "Nice to see ya, pal!".to_owned(),
        Attendee::Person(any_other_name) => format!("Hey, {}.", any_other_name),
    }
}

fn from_division<E>(division_result: Result<f64, E>) -> f64 {
    match division_result {
        Ok(value) => value,
        Err(_) => panic!(), // ... what do we do here?
    }
}

// C# comparisons

struct Point(i32, i32);

fn display(p: Point) -> String {
    match p {
        Point(0, 0) => "origin".to_owned(),
        Point(x, y) => format!("{}, {}", x, y),
    }
}

enum State {
    Opened,
    Closed,
    Locked,
}

enum Transition {
    Close,
    Open,
    Lock,
    Unlock,
}

use State::*;
use Transition::*;

fn change_state(
    current: State,
    transition: Transition,
    has_key: bool,
) -> Result<State, &'static str> {
    match (current, transition) {
        (Opened, Close) => Ok(Closed),
        (Closed, Open) => Ok(Opened),
        (Closed, Lock) if has_key => Ok(Locked),
        (Locked, Unlock) if has_key => Ok(Closed),
        _ => Err("Invalid transition"),
    }
}

enum MyResult<T, E> {
    Ok(T),
    Err(E),
}

fn function<T, E>(result: Result<T, E>) -> &'static str {
    match result {
        Ok(_) => "Ok",
        Err(_) => "Err",
    }
}

enum SchrodingerCat {
    Alive(u8),
    Dead,
}

use SchrodingerCat::*;
fn check_cat(cat: SchrodingerCat) -> String {
    match cat {
        Alive(lives) => format!("The cat's got {} extra lives left.", lives),
        Dead => "The cat is dead.".to_owned(),
    }
}

fn lives_left(cat: SchrodingerCat) -> u8 {
    match cat {
        Alive(lives_left) => lives_left + 1,
        Dead => 0,
    }
}

fn from_lives(lives: u8) -> SchrodingerCat {
    match lives {
        0 => Dead,
        n => Alive(n - 1),
    }
}
