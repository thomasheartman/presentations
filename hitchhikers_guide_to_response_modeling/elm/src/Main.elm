module Main exposing (main, stopMeCompiler)


main =
    stopMeCompiler <| Just 42


stopMeCompiler : Maybe a -> a
stopMeCompiler maybe =
    case maybe of
        Just a ->
            a
